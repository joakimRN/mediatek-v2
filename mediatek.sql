DROP SCHEMA IF EXISTS mediatek_db;
CREATE SCHEMA mediatek_db;
USE mediatek_db;

-- Tables related to media
DROP TABLE IF EXISTS Book;
DROP TABLE IF EXISTS ReadBook;
DROP TABLE IF EXISTS DigitalDisc;
DROP TABLE IF EXISTS Magazine;
DROP TABLE IF EXISTS ShoppingList;
-- Tables related to school
DROP TABLE IF EXISTS Semester;
DROP TABLE IF EXISTS SchoolBook;
DROP TABLE IF EXISTS Course;
DROP TABLE IF EXISTS Grades;

-- Tables
CREATE TABLE Book (
    ISBN CHAR(13),
    Title VARCHAR(50),
    Edition CHAR(4),
    Author VARCHAR(70),
    Publisher VARCHAR(70),
    NumberOfPages CHAR(4),
    Cover VARCHAR(10),
    Genre VARCHAR(30),
    CONSTRAINT BookPK PRIMARY KEY (ISBN)

);

CREATE TABLE ReadBook (
    ISBN CHAR(13),
    Started DATE,
    Finished DATE,
    CONSTRAINT ReadBookPK PRIMARY KEY (ISBN, Started),
    CONSTRAINT ReadBookBookFK FOREIGN KEY (ISBN), REFERENCES Book(ISBN)
);

CREATE TABLE DigitalDisc (
    Title VARCHAR(50),
    AgeRating CHAR(2),
    Genre VARCHAR(30),
    Medium VARCHAR(15),
    CONSTRAINT DigitalDiscPK PRIMARY KEY (Title)
);

CREATE TABLE Magazine (); -- Skipping this?
CREATE TABLE ShoppingList (
    Title VARCHAR(100),
    ProducedBy VARCHAR(100),
    TheType VARCHAR(15),
    Acquired TINYINT
    CONSTRAINT ShoppingListPK PRIMARY KEY (Title, TheType)
);

-- Tables related to school
CREATE TABLE Course ( -- The courses
    CourseCode CHAR(8),
    CourseName VARCHAR(50),
    Grade CHAR(1),
    WeightPoints DECIMAL(3,1),
    Semester CHAR(1),
    CONSTRAINT CoursePK PRIMARY KEY (CourseCode)
    CONSTRAINT CourseSemesterFK FOREIGN KEY Semester REFERENCES Semester(Semester)
);

CREATE TABLE Semester ( -- Keep semesters
    Semester CHAR(1), -- 6 for bachelor, usually
    TheYear CHAR(5), -- Thought I'd write it like this for spring (no: vår) 2020: V2020
    CONSTRAINT SemesterPK PRIMARY KEY (Semester)
);

CREATE TABLE SchoolBook ( -- Links Book and Course
    ISBN CHAR(13),
    CourseCode CHAR(8),
    CONSTRAINT SchoolBookPK PRIMARY KEY (ISBN CourseCode),
    CONSTRAINT SchoolBookBookFK FOREIGN KEY (ISBN) REFERENCES Book(ISBN),
    CONSTRAINT SchoolBookCourseFK FOREIGN KEY (CourseCode) REFERENCES Course(CourseCode)
);

CREATE TABLE GradeLog ( -- Logs changes to grades (done to test triggers and PL/SQL
    TheDate TIMESTAMP,
    CourseCode CHAR(8),
    OldGrade CHAR(1),
    NewGrade CHAR(1),
    CONSTRAINT GradeLogPK PRIMARY KEY (TheDate, CourseCode),
    CONSTRAINT GradeLogCourseFK FOREIGN KEY (CourseCode) REFERENCES Course(CourseCode)
);
